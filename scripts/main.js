let createNewUser = () => {
  let firstName = prompt('Введіть ваше імя');
  let lastName = prompt('Введіть ваше призвіще');

  let birthday = prompt("Введіть  дату вашого народження в  форматі dd.mm.yyyy");
  
  let newUser = {
    firstName,
    lastName,
    birthday,
    getLogin(){
      let sliceFirstLetter = this.firstName.slice(0, 1);
      let firstLetterName = sliceFirstLetter + this.lastName;

      return firstLetterName.toLowerCase();
    },

    getAge() {
      let now = new Date();
      let currentYear = now.getFullYear();
    
      let inputDate = +this.birthday.substring(0, 2);
      let inputMonth = +this.birthday.substring(3, 5);
      let inputYear = +this.birthday.substring(6, 10);
    
      let birthDate = new Date(inputYear, inputMonth-1, inputDate);
      let birthYear = birthDate.getFullYear();
      let age = (currentYear - birthYear) - 1;

      return age;
    },

    getPassword() {

      return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.substring(6,10);
    },

    setFirstName(newFirstName) {
      Object.defineProperty(this, 'firstName', {
        writable: true,
      })

      this.newFirstName = firstName;

      Object.defineProperty(this, 'firstName', {
        writable: false,
      })
      return this.newFirstName
    }

  }

  Object.defineProperties(newUser, {
    firstName: {
      writable: false,
    },
    lastName: {
      writable: false,
    }
  });

  return newUser
}


let user = createNewUser();
// console.log(user.getLogin());

console.log(user.getAge())
console.log(user.getPassword())










